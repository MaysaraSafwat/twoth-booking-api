var express = require('express')
var cors = require('cors')
var bp = require('body-parser')
var mongoose = require("mongoose");
var Router = require("./routes")
var app = express()
var check = require("./services/config");
const envDotProp = require('env-dot-prop');
const {
  setIntervalAsync,
  clearIntervalAsync
} = require('set-interval-async/dynamic')
require('dotenv').config();
const uri = process.env.MONGO_URI;
//const axios = require("axios")

app.use(cors())
app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))
app.use(Router);



mongoose.connect(uri,
  {
    useNewUrlParser: true,
    //useFindAndModify: false,
    useUnifiedTopology: true
  }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});


const data = async ( ) => {
  try{
  const token = await check();
    envDotProp.set("access.token" , token);
    console.log(envDotProp.get("access.token") + " hahahaha")}
    catch(e){
      console.log(e)
    }
}
data();

const timer = setIntervalAsync( //calling it after 2 min from the running the server
  async () => {
    try{
      const token = await check();
      envDotProp.set("access.token" , token);
      console.log(envDotProp.get("access.token") + " hahahaha")}
       catch(e){
        console.log(e)
       }
  },
  3600000 * 6
)


app.listen(8001, function () {
  console.log('CORS-enabled web server listening on port 8001')
})