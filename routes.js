const express = require("express");
const User = require("./models/User");
const app = express();
const axios = require("axios")
const envDotProp = require("env-dot-prop");
const cors = require("cors")
require('dotenv').config();
const stripe = require("stripe")(process.env.STRIPE_SECRET_TEST)





// Stripe Payemnt ==========
app.post("/strippayment", cors(), async (req, res) => {
	let { amount, id } = req.body
	try {
		const payment = await stripe.paymentIntents.create({
			amount,
			currency: "USD",
			description: "Twooth bookings",
			payment_method: id,
			confirm: true
		})
		console.log("Payment", payment)
		res.json({
			message: "Payment successful",
			success: true
		})
	} catch (error) {
		console.log("Error", error)
		res.json({
			message: "Payment failed",
			success: false
		})
	}
})

//===============

//TO GET PRACTICES
app.get("/getPractices", async (req, res) => {
  
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("called!")
    // let token = await check();
     console.log(acctoken)
    let config = {
      headers: {
        Authorization:    `Bearer ${acctoken}`
      }}
      console.log(config.headers.Authorization)
		const response = await axios.get("https://sfd.co:706/practices", config)
    
    res.status(200).send(response.data);
	} catch (err) {
    console.log(err)
		res.status(500).json({ message: err });
	}
});

//TO GET TREATMENTS
app.post("/getPractice", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("called practice!")
    console.log(req.body)
    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database: req.body.database,
        practice: req.body.practice
      }}
		const response = await axios.get("https://sfd.co:706/practice", config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});

//TO GET AVAILABLE DATES
app.post("/getDates", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("dates called")
    console.log(req.body)
    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database: req.body.database,
        practice: req.body.practice,
        reason:req.body.reason,
        year:req.body.year,
        month:req.body.month
      }}
		const response = await axios.get("https://sfd.co:706/dates", config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
  
		res.status(500).json({ message: err });
	}
});

//TO GET AVAIALABLE TIMES
app.post("/getTimes", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("times called")
    console.log(req.body)
    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database: req.body.database,
        practice: req.body.practice,
        reason:req.body.reason,
        date:req.body.date
      }}
		const response = await axios.get("https://sfd.co:706/times", config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});

//TO RESERVE AN APPOINTMENT 
app.post("/reserveAppointment", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("reserve called")
    console.log(acctoken)
    console.log(req.body)
    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database: req.body.database,
        practice: req.body.practice,
        reason:req.body.reason,
        date:req.body.date,
        time: req.body.time
      }}
		const response = await axios.post("https://sfd.co:706/reserve", {} ,config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
	}
});


//TO REGISTER A USER
app.post("/register", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("register called")
    console.log(req.body)
    const { 
     database, practice, firstname,lastname, email, title,
     number,gender,dob, street,city,county,postcode
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        forename:  firstname,
        surname: lastname,
        title:  title,
        gender:  gender, 
        dob: dob,
        street: street,
        city: city,
        county: county,
        postcode: postcode,
        mobile: number,
        email:  email
      }}
		const response = await axios.post("https://sfd.co:706/register", {} ,config)
    console.log(response);
    res.status(200).send(response.data);
    
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


//TO PAIR A USER
app.post("/pair", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("pair called")
    console.log(req.body)
    console.log(acctoken)
    const {
     database, practice,
     session_token,
    pair_method
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        session : session_token,
        pair: pair_method
      }}
		const response = await axios.post("https://sfd.co:706/pair", {},config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


// TO VALIDATE A PAIRING CODE
app.post("/code", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("code called")
    console.log(req.body)
    const {
     database, practice,
     session_token,
    code, email, number
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        session : session_token,
        code: code
      }}
		const response = await axios.post("https://sfd.co:706/code",{}, config)
    console.log(response.data);
     const user = await User.create({
      email : email,
      number: number,
      forename :  response.data.forename,
      surname : response.data.surname,
      dob: response.data.dob,
      gender : response.data.gender,
      patient : response.data.patient,
      title: response.data.title
     })
    user.save()
    res.status(200).send(response.data);
    
  
 } catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


 //PAYMENT
 app.post("/payment", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("payment called")
    console.log(req.body)
    const {
     database, practice,
     session_token,
    payment_token,
    value
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        session : session_token,
        token: payment_token,
        amount: value
      }}
		const response = await axios.post("https://sfd.co:706/payment", {},config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});



 // TO BOOK AN APPOINTMENT
 app.post("/book", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("book called")
    console.log(req.body)
    const {
     database, practice,
     session_token,
     appointment_id,
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        session : session_token,
        appointment : appointment_id
      }}
		const response = await axios.post("https://sfd.co:706/book",{}, config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


// PATIENT SIGN IN 
app.post("/signin", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("signin called")
    console.log(req.body)
    const {
     database, practice,
    firstname,lastname,dob
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        forename : firstname,
        surname: lastname,
        dob: dob
      }}
		const response = await axios.post("https://sfd.co:706/signin",{}, config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


// TO GET PATIENT
app.post("/patient", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("patient called")
    console.log(req.body)
    const {
     database, practice,
    session_token
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`, 
        database:  database,
        practice:  practice,
        session: session_token
      }}
      console.log(acctoken)
		const response = await axios.get("https://sfd.co:706/patient", config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err)
	}
});


// TO SIGN OUT
app.post("/signout", async (req, res) => {
 
	try {
    const acctoken = envDotProp.get("access.token")
    console.log("signout called")
    console.log(req.body)
    const {
     database, practice,
     session_token
    } = req.body;

    let config = {
      headers: {
        Authorization:  `Bearer ${acctoken}`,
        database:  database,
        practice:  practice,
        session: session_token
      }}
		const response = await axios.post("https://sfd.co:706/signout",{}, config)
    console.log(response);
    res.status(200).send(response.data);
	} catch (err) {
		res.status(500).json({ message: err });
    console.log(err);
	}
});


module.exports = app;