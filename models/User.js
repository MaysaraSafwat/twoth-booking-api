const mongoose = require("mongoose");
//const validator = require("validator");

const User = new mongoose.Schema({
  email: {
    type: String,
    trim: true,
    required: true,
   
  },
surname: { type : String, required : true},
forename: {type: String , required : true},
dob : {type: String , required : true},

password: {
    type: String,
    trim: true,
  },
gender : {type : String} ,
mobile : {type : Number},
patient : {type : Number},
bookings: [{ type: mongoose.Schema.Types.ObjectId, ref: "Booking" }],
 
});

module.exports = mongoose.model("User", User);
