const mongoose = require("mongoose");

const Booking = new mongoose.Schema(
  {
   patient_id: { type: mongoose.Schema.Types.ObjectId, ref: "User" }, 
   treatment : {type:String},
   clinician : {type:String},
   date : {type : String},
   time : {type : String},
   appointment_id : {type : Number},
   payment : {type : Number},
   isBooked : { type: Boolean, default: false},
   

  },
  { timestamps: true }
);

module.exports = mongoose.model("Bookings", Booking);