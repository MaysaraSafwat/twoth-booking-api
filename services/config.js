 const envDotProp = require('env-dot-prop');
  const axios = require ("axios")
  require('dotenv').config();
  const id = process.env.API_ID;
  const secret = process.env.API_SECRET;
  //let refresh_token = envDotProp.get("refresh.token")
  //let access_token= envDotProp.get("access.token")
  let refresh_token = process.env.REFRESH_TOKEN;
  let access_token= process.env.ACCESS_TOKEN;
  

  
  module.exports =  async function intervalChek (){

    const params = {
            grant_type :  "refresh_token",
            client_id : id,
            client_secret :  secret,
           refresh_token :refresh_token
    }
  
    console.log("pre-req==>" , process.env.ACCESS_TOKEN , process.env.REFRESH_TOKEN)
  const str = `grant_type=${params.grant_type}&client_id=${params.client_id}&client_secret=${params.client_secret}&refresh_token=${params.refresh_token}`
  const url = "https://sfd.co:706/token?" + str
  const res =   await axios.post(url , null)
  envDotProp.set("refresh.token" , res.data.refresh_token);
  console.log("post-req ==>",process.env.ACCESS_TOKEN , process.env.REFRESH_TOKEN)
  
  return res.data.access_token;
  }
  
